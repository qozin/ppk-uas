<?php
require "koneksi.php";

$response = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $nama = $_POST["nama_barang"];
    $harga = $_POST["harga"];
    $stok = $_POST["stok"];

    $perintah = "INSERT INTO barang (nama_barang, harga, stok) VALUES('$nama','$harga','$stok')";
    $eksekusi = mysqli_query($konek, $perintah);
    $cek = mysqli_affected_rows($konek);

    if ($cek > 0) {
        $response["kode"] = 1;
        $response["pesan"] = "Barang berhasil ditambahkan";
    } else {
        $response["kode"] = 0;
        $response["pesan"] = "Gagal menambahkan barang";
    }
} else {
    $response["kode"] = 0;
    $response["pesan"] = "Tidak ada post barang";
}

echo json_encode($response);
mysqli_close($konek);
