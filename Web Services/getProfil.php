<?php
require "koneksi.php";

$response = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $id = $_POST["id"];

    $perintah = "SELECT * FROM users WHERE id = '$id'";
    $eksekusi = mysqli_query($konek, $perintah);
    $cek = mysqli_affected_rows($konek);

    if ($cek > 0) {
        $response["kode"] = 1;
        $response["pesan"] = "User ditemukan";
        $response["akun"] = array();

        while ($ambil = mysqli_fetch_object($eksekusi)) {
            $F["id"] = $ambil->id;
            $F["nama_lengkap"] = $ambil->nama_lengkap;
            $F["username"] = $ambil->username;
            $F["email"] = $ambil->email;
            $F["password"] = $ambil->kata_sandi;
            $F["role"] = $ambil->peran;

            array_push($response["akun"], $F);
        }
    } else {
        $response["kode"] = 0;
        $response["pesan"] = "User tidak ditemukan";
    }
} else {
    $response["kode"] = 0;
    $response["pesan"] = "Tidak ada post data";
}

echo json_encode($response);
mysqli_close($konek);
