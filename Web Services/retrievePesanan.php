<?php
require "koneksi.php";
$perintah = "SELECT * FROM pesanan";
$eksekusi = mysqli_query($konek, $perintah);
$cek = mysqli_affected_rows($konek);

if ($cek > 0) {
    $response["kode"] = 1;
    $response["pesan"] = "Daftar Pesanan";
    $response["pesanan"] = array();
 
    while ($ambil = mysqli_fetch_object($eksekusi)) {
        $F["id"] = $ambil->id;
        $F["nama_barang"] = $ambil->nama_barang;
        $F["pemesan"] = $ambil->pemesan;
        $F["jumlah"] = $ambil->jumlah;

        array_push($response["pesanan"], $F);
    }

} else {
    $response["kode"] = 0;
    $response["pesan"] = "Tidak Ada Pesanan";
}

echo json_encode($response);
mysqli_close($konek);
