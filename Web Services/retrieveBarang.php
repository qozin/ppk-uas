<?php
require "koneksi.php";
$perintah = "SELECT * FROM barang";
$eksekusi = mysqli_query($konek, $perintah);
$cek = mysqli_affected_rows($konek);
if ($cek > 0) {
    $response["kode"] = 1;
    $response["pesan"] = "Barang Tersedia";
    $response["barang"] = array();
    while ($ambil = mysqli_fetch_object($eksekusi)) {
        $F["id"] = $ambil->id;
        $F["nama_barang"] = $ambil->nama_barang;
        $F["harga"] = $ambil->harga;
        $F["stok"] = $ambil->stok;
        array_push($response["barang"], $F);
    }
} else {
    $response["kode"] = 0;
    $response["pesan"] = "Barang Tidak Tersedia";
}

echo json_encode($response);
mysqli_close($konek);
