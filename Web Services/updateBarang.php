<?php
require "koneksi.php";

$response = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $id = $_POST["id"];
    $harga = $_POST["harga"];
    $stok = $_POST["stok"];

    $perintah = "UPDATE barang SET harga = '$harga', stok = '$stok' WHERE id = '$id'";
    $eksekusi = mysqli_query($konek, $perintah);
    $cek = mysqli_affected_rows($konek);

    if ($cek > 0) {
        $response["kode"] = 1;
        $response["pesan"] = "Data barang berhasil diubah";
    } else {
        $response["kode"] = 0;
        $response["pesan"] = "Data barang gagal diubah";
    }
} else {
    $response["kode"] = 0;
    $response["pesan"] = "Tidak Ada Post Data";
}

echo json_encode($response);
mysqli_close($konek);
