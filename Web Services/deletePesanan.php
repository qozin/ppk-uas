<?php
require "koneksi.php";

$response = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $id = $_POST["id"];

    $perintah = "SELECT * FROM pesanan WHERE id = '$id'";
    $eksekusi = mysqli_query($konek, $perintah);
    $cek = mysqli_affected_rows($konek);
    if ($cek > 0) {
        while ($ambil = mysqli_fetch_object($eksekusi)) {
            $nama = $ambil->nama_barang;
            $jumlah = $ambil->jumlah;
        }
        $perintah = "DELETE FROM pesanan WHERE id = '$id'";
        $eksekusi = mysqli_query($konek, $perintah);
        $perintah = "SELECT * FROM barang WHERE nama_barang = '$nama'";
        $eksekusi = mysqli_query($konek, $perintah); 
        while ($ambil = mysqli_fetch_object($eksekusi)) {
            $stok = $ambil->stok;
        }
        $new = $stok + $jumlah;
        $perintah = "UPDATE barang SET stok = '$new' WHERE nama_barang = '$nama'";
        $eksekusi = mysqli_query($konek, $perintah);
        $response["kode"] = 1;
        $response["pesan"] = "Pesanan berhasil dihapus";
    } else {
        $response["kode"] = 0;
        $response["pesan"] = "Gagal menghapus pesanan";
    }
} else {
    $response["kode"] = 0;
    $response["pesan"] = "Tidak Ada Post Data";
}

echo json_encode($response);
mysqli_close($konek);
