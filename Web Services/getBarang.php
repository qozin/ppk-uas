<?php
require "koneksi.php";

$response = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $id = $_POST["id"];

    $perintah = "SELECT * FROM barang WHERE id = '$id'";
    $eksekusi = mysqli_query($konek, $perintah);
    $cek = mysqli_affected_rows($konek);

    if ($cek > 0) {
        $response["kode"] = 1;
        $response["pesan"] = "Barang Tersedia";
        $response["barang"] = array();

        while ($ambil = mysqli_fetch_object($eksekusi)) {
            $F["id"] = $ambil->id;
            $F["nama_barang"] = $ambil->nama_barang;
            $F["harga"] = $ambil->harga;
            $F["stok"] = $ambil->stok;

            array_push($response["barang"], $F);
        }
    } else {
        $response["kode"] = 0;
        $response["pesan"] = "Data barang tidak tersedia";
    }
} else {
    $response["kode"] = 0;
    $response["pesan"] = "Tidak ada post data";
}

echo json_encode($response);
mysqli_close($konek);
