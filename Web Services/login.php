<?php
require "koneksi.php";

$response = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $username = $_POST["username"];
    $password = $_POST["password"];
    $perintah = "SELECT * FROM users WHERE username = '$username' AND kata_sandi = '$password'";
    $eksekusi = mysqli_query($konek, $perintah);
    $cek = mysqli_affected_rows($konek);
    if ($cek > 0) {
        $response["kode"] = 1;
        $response["pesan"] = "Login berhasil";
        $response["akun"] = array();
        while ($ambil = mysqli_fetch_object($eksekusi)) {
            $F["id"] = $ambil->id;
            $F["username"] = $ambil->username;
            $F["nama"] = $ambil->nama_lengkap;
            $F["email"] = $ambil->email;
            $F["password"] = $ambil->kata_sandi;
            $F["role"] = $ambil->peran;
            array_push($response["akun"], $F);
        }
    } else{
        $response["kode"] = 0;
        $response["pesan"] = "Isikan username dan password dengan benar!";
    } 

} else {
    $response["kode"] = 0;
    $response["pesan"] = "Formulir login belum terisi";
}

echo json_encode($response);
mysqli_close($konek);
