package com.example.e_stock.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import com.example.e_stock.API.RESTAPI;
import com.example.e_stock.API.RetroServer;
import com.example.e_stock.Activity.CustomerPesananActivity;
import com.example.e_stock.Activity.OfficerPesananActivity;
import com.example.e_stock.Model.PesananModel;
import com.example.e_stock.Model.PesananResponseModel;
import com.example.e_stock.R;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterPesananCustomer extends RecyclerView.Adapter<AdapterPesananCustomer.HolderData>{
    private Context ctx;
    private List<PesananModel> listPesanan;
    private Integer idPesanan;

    public AdapterPesananCustomer(Context ctx, List<PesananModel> listPesanan) {
        this.ctx = ctx;
        this.listPesanan = listPesanan;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_pesanan_customer, parent, false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterPesananCustomer.HolderData holder, int position) {
        PesananModel pm = listPesanan.get(position);
        holder.tvid.setText(String.valueOf(pm.getId()));
        holder.tvNama.setText("Barang : "+pm.getNama_barang());
        holder.tvJumlah.setText("Jumlah : "+pm.getJumlah());
    }

    @Override
    public int getItemCount() {
        return listPesanan.size();
    }

    public class HolderData extends RecyclerView.ViewHolder{
        TextView tvid, tvNama, tvJumlah;

        public HolderData(@NonNull View itemView) {
            super(itemView);
            tvid = itemView.findViewById(R.id.tv_id);
            tvNama = itemView.findViewById(R.id.tv_nama);
            tvJumlah = itemView.findViewById(R.id.tv_jumlah);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    AlertDialog.Builder dialogPesan = new AlertDialog.Builder(ctx);
                    dialogPesan.setMessage("Apakah Anda ingin membatalkan pesanan?");
                    dialogPesan.setTitle("Perhatian");
                    dialogPesan.setIcon(R.mipmap.ic_launcher_round);
                    dialogPesan.setCancelable(true);

                    idPesanan = Integer.parseInt(tvid.getText().toString());

                    dialogPesan.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            deletePesanan();
                            dialogInterface.dismiss();
                            Handler hand = new Handler();
                            hand.postDelayed(new Runnable() {
                                @Override
                                public void run() {((CustomerPesananActivity) ctx).retrievePesananCustomer();
                                }
                            }, 1000);
                        }
                    });

                    dialogPesan.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });

                    dialogPesan.show();

                    return false;
                }
            });

        }
        private void deletePesanan(){
            RESTAPI ardPesanan = RetroServer.konekRetrofit().create(RESTAPI.class);
            Call<PesananResponseModel> hapusPesanan = ardPesanan.ardDeletePesanan(idPesanan);

            hapusPesanan.enqueue(new Callback<PesananResponseModel>() {
                @Override
                public void onResponse(Call<PesananResponseModel> call, Response<PesananResponseModel> response) {
                    int kode = response.body().getKode();
                    String pesan = response.body().getPesan();

                    Toast.makeText(ctx, pesan, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<PesananResponseModel> call, Throwable t) {
                    Toast.makeText(ctx, "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
