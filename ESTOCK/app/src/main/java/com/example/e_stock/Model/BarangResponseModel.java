package com.example.e_stock.Model;

import java.util.List;

public class BarangResponseModel {
    private int kode;
    private String pesan;
    private List<BarangModel> barang;

    public int getKode() {
        return kode;
    }

    public void setKode(int kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public List<BarangModel> getBarang() {
        return barang;
    }

    public void setBarang(List<BarangModel> barang) {
        this.barang = barang;
    }
}
