package com.example.e_stock.Activity;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.e_stock.API.RESTAPI;
import com.example.e_stock.API.RetroServer;
import com.example.e_stock.Model.UsersModel;
import com.example.e_stock.Model.UsersResponseModel;
import com.example.e_stock.R;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private EditText usr,pass;
    private Button login,register;
    private String username, password;
    private List<UsersModel> listDataUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usr = (EditText) findViewById(R.id.usr);
        pass = (EditText) findViewById(R.id.pass);
        login = (Button) findViewById(R.id.login);
        register = (Button) findViewById(R.id.register);
    }
    public void doLogin(View view) {
        if(usr.getText().toString().matches("")||pass.getText().toString().matches("")){
            Toast.makeText(MainActivity.this, "Isikan username dan password!", Toast.LENGTH_SHORT).show();
        } else {
            username = usr.getText().toString();
            password = pass.getText().toString();
            login();
        }
    }
    public void doRegister(View view) {
        Intent i = new Intent(MainActivity.this, RegisterActivity.class);
        startActivity(i);
    }
    public void login(){
        RESTAPI ardUsers = RetroServer.konekRetrofit().create(RESTAPI.class);
        Call<UsersResponseModel> login = ardUsers.ardLogin(username,password);

        login.enqueue(new Callback<UsersResponseModel>() {
            @Override
            public void onResponse(Call<UsersResponseModel> call, Response<UsersResponseModel> response) {
                String pesan = response.body().getPesan();
                listDataUser = response.body().getAkun();

                int varIdUser = listDataUser.get(0).getId();
                String varRole = listDataUser.get(0).getRole();

                Intent kirim = new Intent(MainActivity.this, MenuActivity.class);
                kirim.putExtra("UID", varIdUser);
                kirim.putExtra("URole", varRole);
                startActivity(kirim);
                Toast.makeText(MainActivity.this, pesan, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<UsersResponseModel> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}