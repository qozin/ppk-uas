package com.example.e_stock.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.example.e_stock.API.RESTAPI;
import com.example.e_stock.API.RetroServer;
import com.example.e_stock.Adapter.AdapterPesanan;
import com.example.e_stock.Model.PesananResponseModel;
import com.example.e_stock.Model.PesananModel;
import com.example.e_stock.R;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OfficerPesananActivity extends AppCompatActivity {

    private RecyclerView rvPesanan;
    private RecyclerView.Adapter adPesanan;
    private RecyclerView.LayoutManager lmPesanan;
    private List<PesananModel> listPesanan = new ArrayList<>();
    private SwipeRefreshLayout srlPesanan;
    private ProgressBar pbPesanan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_officer_pesanan);
        srlPesanan = findViewById(R.id.srl_pesanan);
        rvPesanan = findViewById(R.id.rv_pesanan);
        pbPesanan = findViewById(R.id.pb_pesanan);
        lmPesanan = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rvPesanan.setLayoutManager(lmPesanan);
        srlPesanan.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srlPesanan.setRefreshing(true);
                retrievePesanan();
                srlPesanan.setRefreshing(false);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        retrievePesanan();
    }

    public void retrievePesanan(){
        pbPesanan.setVisibility(View.VISIBLE);
        RESTAPI ardPesanan = RetroServer.konekRetrofit().create(RESTAPI.class);
        Call<PesananResponseModel> tampilPesanan = ardPesanan.ardRetrievePesanan();
        tampilPesanan.enqueue(new Callback<PesananResponseModel>() {
            @Override
            public void onResponse(Call<PesananResponseModel> call, Response<PesananResponseModel> response) {
                listPesanan = response.body().getPesanan();
                adPesanan = new AdapterPesanan(OfficerPesananActivity.this,listPesanan);
                rvPesanan.setAdapter(adPesanan);
                adPesanan.notifyDataSetChanged();
                pbPesanan.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<PesananResponseModel> call, Throwable t) {
                Toast.makeText(OfficerPesananActivity.this,"Gagal Koneksi ke Server",Toast.LENGTH_SHORT).show();
                pbPesanan.setVisibility(View.INVISIBLE);
            }
        });
    }
}