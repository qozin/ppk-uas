package com.example.e_stock.Model;

import java.util.List;

public class PesananResponseModel {
    private int kode;
    private String pesan;
    private List<PesananModel> pesanan;

    public int getKode() {
        return kode;
    }

    public void setKode(int kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public List<PesananModel> getPesanan() {
        return pesanan;
    }

    public void setPesanan(List<PesananModel> pesanan) {
        this.pesanan = pesanan;
    }
}
