package com.example.e_stock.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.e_stock.API.RESTAPI;
import com.example.e_stock.API.RetroServer;
import com.example.e_stock.Model.BarangResponseModel;
import com.example.e_stock.Model.PesananResponseModel;
import com.example.e_stock.Model.UsersModel;
import com.example.e_stock.Model.UsersResponseModel;
import com.example.e_stock.R;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahPesananActivity extends AppCompatActivity {
    private int userId, barangId, stok, jumlah;
    private String namaBarang, pemesan, harga, stokt;
    private List<UsersModel> listDataAkun;
    private TextView tnama, tpemesan, tharga, tstok;
    private EditText tjumlah;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_pesanan);
        tpemesan = findViewById(R.id.pemesan);
        tnama = findViewById(R.id.barang);
        tharga = findViewById(R.id.harga);
        tstok = findViewById(R.id.stok);
        tjumlah = findViewById(R.id.jumlah);
        Intent terima = getIntent();
        userId = terima.getIntExtra("idUser",-1);
        barangId = terima.getIntExtra("idBarang",-1);
        harga = terima.getStringExtra("harga");
        stokt = terima.getStringExtra("stok");
        namaBarang = terima.getStringExtra("namaBarang");
        getUser();
        tnama.setText("Nama barang : "+namaBarang);
        tharga.setText("Harga barang : "+harga);
        tstok.setText("Stok yang tersedia : "+stokt);
        stok = Integer.valueOf(stokt);

    }
    public void getUser(){
        RESTAPI ardProfil = RetroServer.konekRetrofit().create(RESTAPI.class);
        Call<UsersResponseModel> profil = ardProfil.ardProfil(userId);

        profil.enqueue(new Callback<UsersResponseModel>() {
            @Override
            public void onResponse(Call<UsersResponseModel> call, Response<UsersResponseModel> response) {
                int kode = response.body().getKode();
                String pesan = response.body().getPesan();
                listDataAkun = response.body().getAkun();
                pemesan = listDataAkun.get(0).getNama_lengkap();
                tpemesan.setText("Pemesan : "+pemesan);
            }

            @Override
            public void onFailure(Call<UsersResponseModel> call, Throwable t) {
                Toast.makeText(TambahPesananActivity.this, "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void tambahPesanan(View view) {
        if (tjumlah.getText().toString().equals("")) {
            Toast.makeText(TambahPesananActivity.this, "Jumlah pesanan harus diisi", Toast.LENGTH_SHORT).show();
        } else{
            jumlah = Integer.valueOf(tjumlah.getText().toString());
            if (jumlah > stok) {
                Toast.makeText(TambahPesananActivity.this, "Jumlah pesanan Anda melebihi stok yang tersedia", Toast.LENGTH_SHORT).show();
            } else{
                pesan();
                finish();
            }

        }
    }
    public void pesan(){
        RESTAPI ardPesanan = RetroServer.konekRetrofit().create(RESTAPI.class);
        Call<PesananResponseModel> tambahPesanan = ardPesanan.ardTambahPesanan(namaBarang,pemesan,jumlah);
        tambahPesanan.enqueue(new Callback<PesananResponseModel>() {
            @Override
            public void onResponse(Call<PesananResponseModel> call, Response<PesananResponseModel> response) {
                String pesan = response.body().getPesan();
                Toast.makeText(TambahPesananActivity.this,pesan,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<PesananResponseModel> call, Throwable t) {
                Toast.makeText(TambahPesananActivity.this,"Koneksi ke server gagal",Toast.LENGTH_SHORT).show();
            }
        });
    }
}