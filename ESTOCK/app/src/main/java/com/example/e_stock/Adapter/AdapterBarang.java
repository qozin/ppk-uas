package com.example.e_stock.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import com.example.e_stock.API.RESTAPI;
import com.example.e_stock.API.RetroServer;
import com.example.e_stock.Activity.OfficerBarangActivity;
import com.example.e_stock.Activity.UbahBarangActivity;
import com.example.e_stock.Model.BarangModel;
import com.example.e_stock.Model.BarangResponseModel;
import com.example.e_stock.R;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterBarang extends RecyclerView.Adapter<AdapterBarang.HolderData> {
    private Context ctx;
    private List<BarangModel> listBarang;
    private List<BarangModel> listDataBarang;
    private Integer idBarang;

    public AdapterBarang(Context ctx, List<BarangModel> listBarang) {
        this.ctx = ctx;
        this.listBarang = listBarang;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_barang, parent, false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        BarangModel bm = listBarang.get(position);
        holder.tvid.setText(String.valueOf(bm.getId()));
        holder.tvNama.setText(bm.getNama_barang());
        holder.tvHarga.setText("Harga : "+bm.getHarga());
        holder.tvStok.setText("Stok : "+bm.getStok());
    }

    @Override
    public int getItemCount() {
        return listBarang.size();
    }

    public class HolderData extends RecyclerView.ViewHolder{
        TextView tvid, tvNama, tvHarga, tvStok;

        public HolderData(@NonNull View itemView) {
            super(itemView);
            tvid = itemView.findViewById(R.id.tv_id);
            tvNama = itemView.findViewById(R.id.tv_nama);
            tvHarga = itemView.findViewById(R.id.tv_harga);
            tvStok = itemView.findViewById(R.id.tv_stok);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    AlertDialog.Builder dialogPesan = new AlertDialog.Builder(ctx);
                    dialogPesan.setMessage("Pilih Operasi yang Akan Dilakukan");
                    dialogPesan.setTitle("Perhatian");
                    dialogPesan.setIcon(R.mipmap.ic_launcher_round);
                    dialogPesan.setCancelable(true);

                    idBarang = Integer.parseInt(tvid.getText().toString());

                    dialogPesan.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            deleteBarang();
                            dialogInterface.dismiss();
                            Handler hand = new Handler();
                            hand.postDelayed(new Runnable() {
                                @Override
                                public void run() {((OfficerBarangActivity) ctx).retrieveBarang();
                                }
                            }, 1000);
                        }
                    });

                    dialogPesan.setNegativeButton("Ubah", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getBarang();
                            dialogInterface.dismiss();
                        }
                    });

                    dialogPesan.show();

                    return false;
                }
            });
        }

        private void deleteBarang(){
            RESTAPI ardBarang = RetroServer.konekRetrofit().create(RESTAPI.class);
            Call<BarangResponseModel> hapusBarang = ardBarang.ardDeleteBarang(idBarang);

            hapusBarang.enqueue(new Callback<BarangResponseModel>() {
                @Override
                public void onResponse(Call<BarangResponseModel> call, Response<BarangResponseModel> response) {
                    int kode = response.body().getKode();
                    String pesan = response.body().getPesan();

                    Toast.makeText(ctx, pesan, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<BarangResponseModel> call, Throwable t) {
                    Toast.makeText(ctx, "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }

        private void getBarang(){
            RESTAPI ardBarang = RetroServer.konekRetrofit().create(RESTAPI.class);
            Call<BarangResponseModel> ambilData = ardBarang.ardGetBarang(idBarang);

            ambilData.enqueue(new Callback<BarangResponseModel>() {
                @Override
                public void onResponse(Call<BarangResponseModel> call, Response<BarangResponseModel> response) {
                    int kode = response.body().getKode();
                    String pesan = response.body().getPesan();
                    listDataBarang = response.body().getBarang();

                    int varIdBarang = listDataBarang.get(0).getId();
                    String varNamaBarang = listDataBarang.get(0).getNama_barang();
                    String varHarga = listDataBarang.get(0).getHarga();
                    String varStok = listDataBarang.get(0).getStok();

                    Intent kirim = new Intent(ctx, UbahBarangActivity.class);
                    kirim.putExtra("xId", varIdBarang);
                    kirim.putExtra("xNama_Barang", varNamaBarang);
                    kirim.putExtra("xHarga", varHarga);
                    kirim.putExtra("xStok", varStok);
                    ctx.startActivity(kirim);
                }

                @Override
                public void onFailure(Call<BarangResponseModel> call, Throwable t) {
                    Toast.makeText(ctx, "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
