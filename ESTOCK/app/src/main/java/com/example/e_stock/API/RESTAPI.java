package com.example.e_stock.API;

import com.example.e_stock.Model.BarangResponseModel;
import com.example.e_stock.Model.PesananResponseModel;
import com.example.e_stock.Model.UsersResponseModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RESTAPI {
    @GET("retrieveBarang.php")
    Call<BarangResponseModel> ardRetrieveBarang();

    @FormUrlEncoded
    @POST("addBarang.php")
    Call<BarangResponseModel> ardAddBarang(
            @Field("nama_barang") String nama_barang,
            @Field("harga") Integer harga,
            @Field("stok") Integer stok
    );

    @FormUrlEncoded
    @POST("deleteBarang.php")
    Call<BarangResponseModel> ardDeleteBarang(
            @Field("id") Integer id
    );

    @FormUrlEncoded
    @POST("updateBarang.php")
    Call<BarangResponseModel> ardUpdateData(
            @Field("id") int id,
            @Field("harga") Integer harga,
            @Field("stok") Integer stok
    );

    @FormUrlEncoded
    @POST("getBarang.php")
    Call<BarangResponseModel> ardGetBarang(
            @Field("id") int id
    );

    @FormUrlEncoded
    @POST("login.php")
    Call<UsersResponseModel> ardLogin(
            @Field("username") String username,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("register.php")
    Call<UsersResponseModel> ardRegister(
            @Field("username") String username,
            @Field("nama_lengkap") String nama_lengkap,
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("getProfil.php")
    Call<UsersResponseModel> ardProfil(
            @Field("id") int id
    );

    @FormUrlEncoded
    @POST("editAkun.php")
    Call<UsersResponseModel> ardEdit(
            @Field("id") int id,
            @Field("nama") String nama,
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("tambahPesanan.php")
    Call<PesananResponseModel> ardTambahPesanan(
            @Field("nama_barang") String nama_barang,
            @Field("pemesan") String pemesan,
            @Field("jumlah") int jumlah
    );

    @GET("retrievePesanan.php")
    Call<PesananResponseModel> ardRetrievePesanan();

    @FormUrlEncoded
    @POST("customerPesanan.php")
    Call<PesananResponseModel> ardCustomerPesanan(
            @Field("id") int id
    );

    @FormUrlEncoded
    @POST("deletePesanan.php")
    Call<PesananResponseModel> ardDeletePesanan(
            @Field("id") Integer id
    );
}
