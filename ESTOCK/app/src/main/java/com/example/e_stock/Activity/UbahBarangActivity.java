package com.example.e_stock.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.e_stock.API.RESTAPI;
import com.example.e_stock.API.RetroServer;
import com.example.e_stock.Model.BarangResponseModel;
import com.example.e_stock.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UbahBarangActivity extends AppCompatActivity {
    private int xId;
    private String xNama, xHarga, xStok;
    private EditText etHarga, etStok;
    private TextView etNama;
    private Button btnUbah;
    private String yNama, yHarga, yStok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_barang);

        Intent terima = getIntent();
        xId = terima.getIntExtra("xId", -1);
        xNama = terima.getStringExtra("xNama_Barang");
        xHarga = terima.getStringExtra("xHarga");
        xStok = terima.getStringExtra("xStok");

        etNama = findViewById(R.id.et_nama);
        etHarga = findViewById(R.id.et_harga);
        etStok = findViewById(R.id.et_stok);
        btnUbah = findViewById(R.id.btn_ubah);

        etNama.setText(xNama);
        etHarga.setText(xHarga);
        etStok.setText(xStok);

        btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                yHarga = etHarga.getText().toString();
                yStok = etStok.getText().toString();

                updateData();
            }
        });
    }

    private void updateData(){
        RESTAPI ardBarang = RetroServer.konekRetrofit().create(RESTAPI.class);
        Call<BarangResponseModel> ubahData = ardBarang.ardUpdateData(xId, Integer.valueOf(yHarga), Integer.valueOf(yStok));

        ubahData.enqueue(new Callback<BarangResponseModel>() {
            @Override
            public void onResponse(Call<BarangResponseModel> call, Response<BarangResponseModel> response) {
                int kode = response.body().getKode();
                String pesan = response.body().getPesan();
                Toast.makeText(UbahBarangActivity.this, pesan, Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(Call<BarangResponseModel> call, Throwable t) {
                Toast.makeText(UbahBarangActivity.this, "Gagal Menghubungi Server | "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}