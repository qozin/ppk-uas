package com.example.e_stock.Activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.e_stock.API.RESTAPI;
import com.example.e_stock.API.RetroServer;
import com.example.e_stock.Model.BarangResponseModel;
import com.example.e_stock.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahBarangActivity extends AppCompatActivity {
    private EditText etNama,etHarga,etStok;
    private Button btnSimpan;
    private String nama, harga, stok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_barang);
        etNama = findViewById(R.id.et_nama);
        etHarga = findViewById(R.id.et_harga);
        etStok = findViewById(R.id.et_Stok);
        btnSimpan = findViewById(R.id.btn_tambah);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nama = etNama.getText().toString();
                harga = etHarga.getText().toString();
                stok = etStok.getText().toString();

                if(nama.trim().equals("")){
                    etNama.setError("Nama barang harus diisi");
                } else if(harga.trim().equals("")){
                    etHarga.setError("Harga harus diisi");
                } else if (stok.trim().equals("")){
                    etStok.setError("Stok harus diisi");
                } else {
                    addBarang();
                    finish();
                }
            }
        });
    }

    private void addBarang(){
        RESTAPI ardBarang = RetroServer.konekRetrofit().create(RESTAPI.class);
        Call<BarangResponseModel> tambahBarang = ardBarang.ardAddBarang(nama,Integer.valueOf(harga),Integer.valueOf(stok));
        tambahBarang.enqueue(new Callback<BarangResponseModel>() {
            @Override
            public void onResponse(Call<BarangResponseModel> call, Response<BarangResponseModel> response) {
                String pesan = response.body().getPesan();
                Toast.makeText(TambahBarangActivity.this,pesan,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<BarangResponseModel> call, Throwable t) {
                Toast.makeText(TambahBarangActivity.this,"Koneksi ke server gagal",Toast.LENGTH_SHORT).show();
            }
        });
    }
}