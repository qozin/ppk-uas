package com.example.e_stock.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.example.e_stock.API.RESTAPI;
import com.example.e_stock.API.RetroServer;
import com.example.e_stock.Adapter.AdapterBarang;
import com.example.e_stock.Model.BarangModel;
import com.example.e_stock.Model.BarangResponseModel;
import com.example.e_stock.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class OfficerBarangActivity extends AppCompatActivity {
    private RecyclerView rvBarang;
    private RecyclerView.Adapter adBarang;
    private RecyclerView.LayoutManager lmBarang;
    private List<BarangModel> listBarang = new ArrayList<>();
    private SwipeRefreshLayout srlBarang;
    private ProgressBar pbBarang;
    private FloatingActionButton fabTambah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_officer_barang);
        srlBarang = findViewById(R.id.srl_barang);
        rvBarang = findViewById(R.id.rv_barang);
        pbBarang = findViewById(R.id.pb_barang);
        fabTambah = findViewById(R.id.fab_tambah);
        lmBarang = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rvBarang.setLayoutManager(lmBarang);
        srlBarang.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srlBarang.setRefreshing(true);
                retrieveBarang();
                srlBarang.setRefreshing(false);
            }
        });
        fabTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(OfficerBarangActivity.this,TambahBarangActivity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        retrieveBarang();
    }

    public void retrieveBarang(){
        pbBarang.setVisibility(View.VISIBLE);
        RESTAPI ardBarang = RetroServer.konekRetrofit().create(RESTAPI.class);
        Call<BarangResponseModel> tampilBarang = ardBarang.ardRetrieveBarang();
        tampilBarang.enqueue(new Callback<BarangResponseModel>() {
            @Override
            public void onResponse(Call<BarangResponseModel> call, Response<BarangResponseModel> response) {
                listBarang = response.body().getBarang();
                adBarang = new AdapterBarang(OfficerBarangActivity.this,listBarang);
                rvBarang.setAdapter(adBarang);
                adBarang.notifyDataSetChanged();
                pbBarang.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<BarangResponseModel> call, Throwable t) {
                Toast.makeText(OfficerBarangActivity.this,"Gagal Koneksi ke Server",Toast.LENGTH_SHORT).show();
                pbBarang.setVisibility(View.INVISIBLE);
            }
        });
    }
}