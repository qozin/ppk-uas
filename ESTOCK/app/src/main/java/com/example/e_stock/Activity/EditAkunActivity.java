package com.example.e_stock.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.e_stock.API.RESTAPI;
import com.example.e_stock.API.RetroServer;
import com.example.e_stock.Model.UsersResponseModel;
import com.example.e_stock.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditAkunActivity extends AppCompatActivity {
    private TextView e_username;
    private EditText e_nama, e_email, e_password, e_passwordb, e_kpasswordb;
    private int id;
    private String username, xnama, xemail, xpassword;
    private String ynama, yemail, ypassword, passwordb, kpasswordb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_akun);
        Intent terima = getIntent();
        id = terima.getIntExtra("id",-1);
        username = terima.getStringExtra("username");
        xnama = terima.getStringExtra("nama");
        xemail = terima.getStringExtra("email");
        xpassword = terima.getStringExtra("password");
        e_username = findViewById(R.id.e_username);
        e_nama = findViewById(R.id.e_nama);
        e_email = findViewById(R.id.e_email);
        e_password = findViewById(R.id.e_password);
        e_passwordb = findViewById(R.id.e_passwordb);
        e_kpasswordb = findViewById(R.id.e_kpasswordb);
        e_username.setText(username);
    }
    public void editAkun(View view){
        ynama = e_nama.getText().toString();
        yemail = e_email.getText().toString();
        ypassword = e_password.getText().toString();
        passwordb = e_passwordb.getText().toString();
        kpasswordb = e_kpasswordb.getText().toString();
        if(ypassword.trim().equals("")||passwordb.trim().equals("")||ynama.trim().equals("")||yemail.trim().equals("")||kpasswordb.trim().equals("")){
            Toast.makeText(EditAkunActivity.this,"Semua field harus diisi",Toast.LENGTH_SHORT).show();
        } else {
            if(ypassword.equals(xpassword)){
                if(passwordb.equals(kpasswordb)){
                    edit();
                } else{
                    Toast.makeText(EditAkunActivity.this,"Konfirmasi password tidak sesuai",Toast.LENGTH_SHORT).show();
                }
            } else{
                Toast.makeText(EditAkunActivity.this,"Password yang Anda masukkan salah",Toast.LENGTH_SHORT).show();
            }
        }
    }
    public void edit(){
        RESTAPI ardAkun = RetroServer.konekRetrofit().create(RESTAPI.class);
        Call<UsersResponseModel> ubahData = ardAkun.ardEdit(id, ynama, yemail, passwordb);

        ubahData.enqueue(new Callback<UsersResponseModel>() {
            @Override
            public void onResponse(Call<UsersResponseModel> call, Response<UsersResponseModel> response) {
                String pesan = response.body().getPesan();
                Toast.makeText(EditAkunActivity.this, pesan, Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(Call<UsersResponseModel> call, Throwable t) {
                Toast.makeText(EditAkunActivity.this, "Gagal Menghubungi Server | "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}