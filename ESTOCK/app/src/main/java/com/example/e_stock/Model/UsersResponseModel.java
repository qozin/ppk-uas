package com.example.e_stock.Model;

import java.util.List;

public class UsersResponseModel {
    private int kode;
    private String pesan;
    private List<UsersModel> akun;

    public int getKode() {
        return kode;
    }

    public void setKode(int kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public List<UsersModel> getAkun() {
        return akun;
    }

    public void setAkun(List<UsersModel> akun) {
        this.akun = akun;
    }
}
