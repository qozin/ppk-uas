package com.example.e_stock.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.example.e_stock.API.RESTAPI;
import com.example.e_stock.API.RetroServer;
import com.example.e_stock.Model.UsersModel;
import com.example.e_stock.Model.UsersResponseModel;
import com.example.e_stock.R;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilActivity extends AppCompatActivity {
    private TextView t_role, t_nama, t_email;
    private int id;
    private String nama, username, role, email, password;
    private List<UsersModel> listDataAkun;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        t_role = findViewById(R.id.t_role);
        t_nama = findViewById(R.id.t_nama);
        t_email = findViewById(R.id.t_email);
        Intent terima = getIntent();
        id = terima.getIntExtra("UID", -1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getProfil();
    }

    public void getProfil(){
        RESTAPI ardProfil = RetroServer.konekRetrofit().create(RESTAPI.class);
        Call<UsersResponseModel> profil = ardProfil.ardProfil(id);

        profil.enqueue(new Callback<UsersResponseModel>() {
            @Override
            public void onResponse(Call<UsersResponseModel> call, Response<UsersResponseModel> response) {
                int kode = response.body().getKode();
                String pesan = response.body().getPesan();
                listDataAkun = response.body().getAkun();

                nama = listDataAkun.get(0).getNama_lengkap();
                email = listDataAkun.get(0).getEmail();
                role = listDataAkun.get(0).getRole();

                t_role.setText(role);
                t_nama.setText(nama);
                t_email.setText(email);
            }

            @Override
            public void onFailure(Call<UsersResponseModel> call, Throwable t) {
                Toast.makeText(ProfilActivity.this, "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void editAkun(View view){
        RESTAPI ardProfil = RetroServer.konekRetrofit().create(RESTAPI.class);
        Call<UsersResponseModel> profil = ardProfil.ardProfil(id);

        profil.enqueue(new Callback<UsersResponseModel>() {
            @Override
            public void onResponse(Call<UsersResponseModel> call, Response<UsersResponseModel> response) {
                int kode = response.body().getKode();
                String pesan = response.body().getPesan();
                listDataAkun = response.body().getAkun();

                id = listDataAkun.get(0).getId();
                nama = listDataAkun.get(0).getNama_lengkap();
                username = listDataAkun.get(0).getUsername();
                email = listDataAkun.get(0).getPassword();
                password = listDataAkun.get(0).getPassword();

                Intent kirim = new Intent(ProfilActivity.this,EditAkunActivity.class);
                kirim.putExtra("id", id);
                kirim.putExtra("username", username);
                kirim.putExtra("nama", nama);
                kirim.putExtra("email", email);
                kirim.putExtra("password", password);
                kirim.putExtra("role", role);
                startActivity(kirim);
            }

            @Override
            public void onFailure(Call<UsersResponseModel> call, Throwable t) {
                Toast.makeText(ProfilActivity.this, "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}