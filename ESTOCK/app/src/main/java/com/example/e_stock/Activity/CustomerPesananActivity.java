package com.example.e_stock.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.e_stock.API.RESTAPI;
import com.example.e_stock.API.RetroServer;
import com.example.e_stock.Adapter.AdapterPesanan;
import com.example.e_stock.Adapter.AdapterPesananCustomer;
import com.example.e_stock.Model.PesananModel;
import com.example.e_stock.Model.PesananResponseModel;
import com.example.e_stock.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerPesananActivity extends AppCompatActivity {

    private RecyclerView rvPesanan;
    private RecyclerView.Adapter adPesanan;
    private RecyclerView.LayoutManager lmPesanan;
    private List<PesananModel> listPesanan = new ArrayList<>();
    private SwipeRefreshLayout srlPesanan;
    private ProgressBar pbPesanan;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_pesanan);
        Intent terima = getIntent();
        id = terima.getIntExtra("UID",-1);
        srlPesanan = findViewById(R.id.srl_pesanan);
        rvPesanan = findViewById(R.id.rv_pesanan);
        pbPesanan = findViewById(R.id.pb_pesanan);
        lmPesanan = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rvPesanan.setLayoutManager(lmPesanan);
        srlPesanan.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srlPesanan.setRefreshing(true);
                retrievePesananCustomer();
                srlPesanan.setRefreshing(false);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        retrievePesananCustomer();
    }

    public void retrievePesananCustomer(){
        pbPesanan.setVisibility(View.VISIBLE);
        RESTAPI ardPesanan = RetroServer.konekRetrofit().create(RESTAPI.class);
        Call<PesananResponseModel> tampilPesanan = ardPesanan.ardCustomerPesanan(id);
        tampilPesanan.enqueue(new Callback<PesananResponseModel>() {
            @Override
            public void onResponse(Call<PesananResponseModel> call, Response<PesananResponseModel> response) {
                listPesanan = response.body().getPesanan();
                adPesanan = new AdapterPesananCustomer(CustomerPesananActivity.this,listPesanan);
                rvPesanan.setAdapter(adPesanan);
                adPesanan.notifyDataSetChanged();
                pbPesanan.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<PesananResponseModel> call, Throwable t) {
                Toast.makeText(CustomerPesananActivity.this,"Gagal Koneksi ke Server"+ t.getMessage(),Toast.LENGTH_SHORT).show();
                pbPesanan.setVisibility(View.INVISIBLE);
            }
        });
    }
}