package com.example.e_stock.Activity;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.e_stock.R;

public class MenuActivity extends AppCompatActivity {
    private Button bPesanan, bBarang, bProfil, bKeluar;
    private int UID;
    private String URole;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        bPesanan = findViewById(R.id.bPesanan);
        bBarang =findViewById(R.id.bBarang);
        bProfil = findViewById(R.id.bProfil);
        bKeluar = findViewById(R.id.mkeluar);
        Intent terima = getIntent();
        UID = terima.getIntExtra("UID", -1);
        URole = terima.getStringExtra("URole");
    }

    public void menuPesanan(View view){
        if (URole.equals("officer")){
            Intent i = new Intent(MenuActivity.this, OfficerPesananActivity.class);
            startActivity(i);
        } else{
            Intent kirim = new Intent(MenuActivity.this, CustomerPesananActivity.class);
            kirim.putExtra("UID",UID);
            startActivity(kirim);
        }
    }

    public void menuBarang(View view){
        if (URole.equals("officer")){
            Intent i = new Intent(MenuActivity.this, OfficerBarangActivity.class);
            startActivity(i);
        } else{
            Intent kirim = new Intent(MenuActivity.this, CustomerBarangActivity.class);
            kirim.putExtra("UID",UID);
            startActivity(kirim);
        }
    }

    public void menuProfil(View view){
        Intent kirim = new Intent(MenuActivity.this, ProfilActivity.class);
        kirim.putExtra("UID",UID);
        startActivity(kirim);
    }

    public void keluar(View view){
        Intent i = new Intent(MenuActivity.this,MainActivity.class);
        startActivity(i);
        Toast.makeText(MenuActivity.this,"Anda berhasil keluar",Toast.LENGTH_SHORT).show();
    }


}