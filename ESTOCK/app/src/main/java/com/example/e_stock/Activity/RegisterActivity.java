package com.example.e_stock.Activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.e_stock.API.RESTAPI;
import com.example.e_stock.API.RetroServer;
import com.example.e_stock.Model.UsersResponseModel;
import com.example.e_stock.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    private EditText rNama, rEmail, rUsername, rPassword, rKPassword;
    private Button rDaftar;
    private String nama, email, username, password, kPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        rNama = (EditText) findViewById(R.id.rNama);
        rEmail = (EditText) findViewById(R.id.rEmail);
        rUsername = (EditText) findViewById(R.id.rUsername);
        rPassword = (EditText) findViewById(R.id.rPassword);
        rKPassword = (EditText) findViewById(R.id.rKPassword);
        rDaftar = (Button) findViewById(R.id.rDaftar);
    }
    public void doDaftar(View view) {
        if(rNama.getText().toString().matches("")||rEmail.getText().toString().matches("")||rUsername.getText().toString().matches("")||rPassword.getText().toString().matches("")||rKPassword.getText().toString().matches("")){
            Toast.makeText(RegisterActivity.this, "Formulir belum terisi sepenuhnya", Toast.LENGTH_SHORT).show();
        } else {
            nama = rNama.getText().toString();
            email = rEmail.getText().toString();
            username = rUsername.getText().toString();
            password = rPassword.getText().toString();
            kPassword = rKPassword.getText().toString();
            if(password.equals(kPassword)){
                register();
                finish();
            } else {
                Toast.makeText(RegisterActivity.this, "Konfirmasi password tidak sesuai",Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void register(){
        RESTAPI ardRegister = RetroServer.konekRetrofit().create(RESTAPI.class);
        Call<UsersResponseModel> register = ardRegister.ardRegister(username,nama,email,password);
        register.enqueue(new Callback<UsersResponseModel>() {
            @Override
            public void onResponse(Call<UsersResponseModel> call, Response<UsersResponseModel> response) {
                String pesan = response.body().getPesan();
                Toast.makeText(RegisterActivity.this,pesan,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<UsersResponseModel> call, Throwable t) {
                Toast.makeText(RegisterActivity.this,"Koneksi ke server gagal",Toast.LENGTH_SHORT).show();
            }
        });
    }
}